## Preliminary Module Overview 

Top Sheet:
    - Shottky diode from 5v to 5v_Backup powers Emergency side when power is available, and prevents backflow when not
    - Boost converter is switched on when 5v input drops low enough to turn on mosfet, and boosts battery voltage up to 5v. Minimum battery voltage of ~0.7v.
    
    
    
Inputs Sheet:
    - TVS Diode arrays protect from ESD. P-mosfets level shifts input logic to 5v.
    - For inputs with level shifting mosfets, the microcontroller must configure GPIO pins as open drain. (Floating translates to logic 0, sink pin to ground for logic 1).
    
    
    
Warning System Sheet:
    - Warning bits go into logic gates to determine the type of warning, and then enables the 555 timer and selects upper resistor for beep rate
    - Warning 555 timer generates the "beep" on the buzzer and at the same time flashes the yellow warning LED. Beep rates are as follows:
    
                            TimeOn      TimeOff     (TimeOn corresponds with seeing the Yellow LED and hearing the warning buzzer)
            Warning Low -   0.353 s     3.125 s
            Warning Med -   0.353 s     1.040 s
            Warning Hi  -   0.353 s     0.360 s
            
    - If Emergency occurs, the Warning circuitry is disabled
    - If Warning is muted, the LED will flash, but the buzzer will not make sound
    - The warning volume level is controlled by an I2C digital potentiometer
        I2C digital pot does not source much current, thus op amp is used as voltage follower
            
            
Emergency System Sheet:
    - Emergency 555 Timer generates Red LED flash and Beep of buzzers when enabeled by either emergency now gpio input, or watchdog timer. 
    - Emergency alarm sounds both buzzers at max volume, Beep rate is as follows:
        
                        TimeOn      TimeOff     (TimeOn corresponds with seeing the Red LED and hearing the Emergency buzzer, while TimeOff corresponds with hearing the Warning Buzzer)
            Emergency - 0.139 s     0.146 s
            
    - All Emergency circuitry runs on 5v backup power, separate from the Warning system
    - Watchdog is enabled by latching the D-Latch to high
            + When disabled, the PNP BJT holds the timing capacitor above the voltage threshold and disable the output via an AND gate
    - When enabled, the microcontroller must send the OK signal at an interval faster than 220ms
    - The Watchdog timer sends a single pulse when timed out, thus two NOR gates are used as SR latch to latch this pulse
    - The nature of the Watchdog timer will set the SR latch to high during powerup, thus the following steps should be taken to avoid faulty emergency alarm:
            + On power up, microcontroller should send a clk pulse to ensure D-latch state as low
            + Before turing on the watchdog circuitry via the D-latch, the system should start by sending a system OK pulse to reset the SR latch to low